class watcher {
    constructor (vm, expr, cb) {
        this.vm = vm
        this.expr = expr
        this.cb = cb
        this.oldVal = this.getOldVal()
    }
    getOldVal () {
        Dep.target = this // 这里没有看懂嘎嘎嘎-----看懂啦compileUtil.getValue（） 方法的过程就触发Object.defineProperty的get的方法了
        const oldVal=  compileUtil.getValue(this.expr, this.vm)
        Dep.target = null // 这里没有看懂嘎嘎嘎嘎嘎
        return oldVal
    }
    updata() { // 首先判断数据有没有变化，有变化的话就回调更新视图的方法
        const newVal=  compileUtil.getValue(this.expr, this.vm)
        if (newVal !== this.oldVal) {
            this.cb(newVal)
        }
    }
}
// 实现订阅器Dep
class Dep {
    constructor () {
        this.subs = [] // 先定义这个Dep
    }
    addSub(watcher) {
        this.subs.push(watcher)
    }
    notify() {
        this.subs.forEach(w => w.updata())
    }
}
class Observer {
    constructor (data) {
        this.observer(data)
    }
    observer (data) {
        if (data && typeof data === 'object') {
            Object.keys(data).forEach(key => {
                this.defineReactive(data, key, data[key])
            })
        }
    }
    defineReactive (object, key, value) {
        this.observer(value)
        const dep = new Dep()
        Object.defineProperty(object, key, {
            enumerable: true, // 是否可遍历
            configurable: false, // 是否可编写的
            get() {
                // 订阅数据变化的时候，往Dep中添加观察着
                Dep.target && dep.addSub(Dep.target)
                return value
            },
            set:(newVal) =>{
                this.observer(newVal)
                if (newVal !== value) {
                    value = newVal
                }
                // 如果更改数据了就告诉Dep通知变化
                dep.notify()
            }
        })
    }
}