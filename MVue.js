

class MVue {
    constructor (options) {
        this.$el = options.el;
        this.$data = options.data;
        this.$options = options;
        if (this.$el) {
            new Observer(this.$data) // 实现数据观察者
            new Compile(this.$el, this) // 实现编译
            // 弄一个代理
            this.proxyData(this.$data)
        }
    }
    proxyData (data) {
        for(const key in data){
            Object.defineProperty(this, key, {
                get() {
                    return data[key]
                },
                set(newVal) {
                    data[key] = newVal
                }
            })
        }
    }
}
const compileUtil = { // 这里是对各个指令进行编译，替换成对应的数据
    getValue (expr,vm) {
        return expr.split('.').reduce((pre, cur) => {
            return pre[cur]
        }, vm.$data)
    },
    setValue (expr, vm, inputVal) { // input双向数据绑定，视图变化的时候取设置数据的值
        return expr.split('.').reduce((pre, cur) => {
            pre[cur] = inputVal
        }, vm.$data)
    },
    getCountentVal (expr, vm) {
        return expr.replace(/\{\{(.+?)\}\}/g, (...args) => { // 这个...args有点看不懂 嘎嘎嘎嘎嘎
            return this.getValue(args[1], vm)
            // new watcher(vm, args[1], (newVal) => {
            //     this.updater.textUpdater(node, this.getCountentVal(expr, vm))
            // }) // 个人总结：在节点上创建watcher
        })
    },
    text(node, expr, vm) { // expr:msg 学习MVVM原理  
        let value;
        if (expr.indexOf('{{') !== -1) { // 含大括号
            
            value = expr.replace(/\{\{(.+?)\}\}/g, (...args) => { // 这个...args有点看不懂 嘎嘎嘎嘎嘎
                // return this.getValue(args[1], vm)
                new watcher(vm, args[1], (newVal) => {
                    this.updater.textUpdater(node, this.getCountentVal(expr, vm))
                }) // 个人总结：在节点上创建watcher
                return this.getValue(args[1], vm)
            })
        } else {
            value = this.getValue(expr, vm)
        }
        this.updater.textUpdater(node, value) 
    },
    html (node, expr, vm) {
        let value = this.getValue(expr, vm)
        new watcher(vm, expr, (newVal) => {
            this.updater.htmlUpdater(node, newVal)
        }) // 个人总结：在节点上创建watcher
        this.updater.htmlUpdater (node, value)
    },
    model (node, expr, vm) {
        let value = this.getValue(expr, vm)
        // 这里是绑定更新函数=》数据改变去改变视图
        new watcher(vm, expr, (newVal) => {
            this.updater.htmlUpdater(node, newVal)
        }) // 个人总结：在节点上创建watcher
        node.addEventListener('input', (e) => {
            console.log('=====', e.target.value)
            this.setValue(expr, vm, e.target.value)
        })
        // 视图变化去改变数据

        this.updater.modelUpdater(node, value)
    },
    on (node, expr, vm, eventName) {
        let fn = vm.$options.methods && vm.$options.methods[expr]
        node.addEventListener(eventName, fn.bind(vm),false)
    },
    updater: {
        textUpdater(node, value) {
            node.textContent = value // 对文本进行赋值
        },
        htmlUpdater(node, value) {
            node.innerHTML = value
        },
        modelUpdater(node, value) {
            node.value = value
        }
    }
}
class Compile {
    constructor (el, vm) {
        this.el = this.isElementNode(el) ? el : document.querySelector(el);
        this.vm = vm;
        //1、获取文档碎片对象  放入内存中减少页面的回流和重绘(个人感觉就是获取虚拟dom)
        const fragment = this.node2Fragent(this.el);
        // 2、编译模板
        this.compile(fragment);
        // 3、追加子元素到根元素上：
        this.el.appendChild(fragment);
    }
    isElementNode (Node) {
        return Node.nodeType === 1; // 判断是否为节点
    }
    node2Fragent (el) {
        const f = document.createDocumentFragment();
        // 循环来创建文档碎片节点 x虚拟dom
        let firstChild;
        while(firstChild = el.firstChild) {
            f.appendChild(firstChild);
        }
        return f;
    }
    compile (fragment) {
        // 获取所有的子节点
        const childNodes = fragment.childNodes;
        [...childNodes].forEach(child => {
            // 判断是否为元素节点
            if(this.isElementNode(child)) {
                // 对元素的节点进行处理
                this.compileElement(child);
            } else {
                // 否则为文本节点 
                // 对文本的节点进行处理  
                this.compileText(child);
            }
            if (child.childNodes && child.childNodes.length) {
                this.compile(child)
            }
        })
    }
    compileElement (node) { // 对元素节点进行编译
        const attributes = node.attributes;
        [...attributes].forEach(attr => {
            // 利用解构赋值把ke和value取出来
            const {name, value} = attr
            if(this.isDirective(name)) { // 判断是否是v- 头的指令 v-text v-html v-model v-on:click
                const [,directive] = name.split('-'); //获取到赤裸裸的指令： text html model o:click
                const [dirName, eventName] = directive.split(':'); // 获取到text html model on
                compileUtil[dirName](node, value, this.vm, eventName); // 这里进行编译赋值
                // 删除页面元素中的v-text等命令
                node.removeAttribute('v-' + directive)
            } else if (this.isEventName(name)) { // 判断是否是@click
                let [, eventName] = name.split('@')
                compileUtil['on'](node, value, this.vm, eventName); // 这里进行编译赋值
            }
        })
    }   
    
    compileText (node) { // 对文本节点进行编译 就是{{}}
        const content = node.textContent;
        if(/\{\{(.+?)\}\}/.test(content)) { // 正则过滤有大括号的
            compileUtil['text'](node, content, this.vm)
        }   
    }
    isDirective (attrName) { // 判断是否为自定义的指令
        return attrName.startsWith('v-');

    }
    isEventName (attrName) {
        return attrName.startsWith('@');
    }
}